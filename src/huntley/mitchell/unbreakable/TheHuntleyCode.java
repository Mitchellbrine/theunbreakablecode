package huntley.mitchell.unbreakable;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by Mitchellbrine on 2015.
 */
public class TheHuntleyCode {


	public static void main(String[] args) {
		String originalText = args[0];
		List<Long> seedList = new ArrayList<Long>();
		long seedValue1 = nextLong(new Random(),1000000000000L);
		long seedValue2 = nextLong(new Random(),1000000000000L);
		long seedValue3 = nextLong(new Random(),1000000000000L);
		long seedValue4 = nextLong(new Random(),1000000000000L);
		long seedValue5 = nextLong(new Random(),1000000000000L);
		long seedValue6 = nextLong(new Random(),1000000000000L);
		long seedValue7 = nextLong(new Random(),1000000000000L);
		long seedValue8 = nextLong(new Random(),1000000000000L);
		long seedValue9 = nextLong(new Random(),1000000000000L);

		seedList.add(seedValue1);
		seedList.add(seedValue2);
		seedList.add(seedValue3);
		seedList.add(seedValue4);
		seedList.add(seedValue5);
		seedList.add(seedValue6);
		seedList.add(seedValue7);
		seedList.add(seedValue8);
		seedList.add(seedValue9);

		long seed = (seedValue1 + seedValue2 + seedValue3 + seedValue4 + seedValue5 + seedValue6 + seedValue7 + seedValue8 + seedValue9) + (new Random().nextInt(26) + 1);

		Random charRandom = new Random(seed);

		StringBuilder stringBuilder = new StringBuilder();

		long insaneValue = 1L;

		for (int ii = 0; ii < 3; ii++) {
			int index = charRandom.nextInt(seedList.size());
			long valueAtPos = seedList.get(index);
			insaneValue = insaneValue + valueAtPos;
			seedList.remove(index);
		}

		for (int i = 0; i < originalText.length(); i++){
			long c = originalText.charAt(i) * (charRandom.nextInt(26) + 1) * insaneValue;
			stringBuilder.append(c + "+");
		}

		stringBuilder.append(seed * nextLong(new Random(),1000000000000L) * (charRandom.nextInt(26) + 1));

		String finalCrypto = stringBuilder.toString();

		System.out.println(finalCrypto);

	}

	public static long nextLong(Random rng, long n) {
		// error checking and 2^x checking removed for simplicity.
		long bits, val;
		do {
			bits = (rng.nextLong() << 1) >>> 1;
			val = bits % n;
		} while (bits-val+(n-1) < 0L);
		return val;
	}

}
